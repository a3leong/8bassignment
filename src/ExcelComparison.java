/**
 * @authors: Jelissa Bueza && Hyo Na Lee
 * @PID: A113260671 && A11729078
 * File: ExcelComparison.java
 * Date: 11/21/2015
 * 
 * Purpose: Compare 2 excel sheets and compares SKU and MAP values.
 * 				outputs a new excel sheet with updated information.
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ExcelComparison extends Application {



	//start method implements new variables to create a GUI
    @Override
    public void start(Stage stage) {
        Group root = new Group();
        Scene scene = new Scene(root, 300, 150);
        stage.setScene(scene);
        stage.setTitle("Compare Files");

        GridPane grid = new GridPane();
        grid.setVgap(5);
        grid.setHgap(5);

        scene.setRoot(grid);
        
        FileChooser fileChooser = new FileChooser();
        FileChooser secondFileChooser = new FileChooser();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        

        // Creates Text Fields for the GUI showing the FilePath
        TextField input = new TextField();
        input.setPromptText("Enter Master File");
        input.getText();
        GridPane.setConstraints(input, 0, 0);
        grid.getChildren().add(input);

        TextField input2 = new TextField();
        input2.setPromptText("Enter Second File");
        GridPane.setConstraints(input2, 0, 1);
        grid.getChildren().add(input2);

        TextField input3 = new TextField();
        input3.setPrefColumnCount(15);
        input3.setPromptText("Enter New Destination");
        GridPane.setConstraints(input3, 0, 2);
        grid.getChildren().add(input3);

        
        // Creates Buttons for the GUI to open files, open the destination folder, and run the program
        Button open1 = new Button("Open");
        GridPane.setConstraints(open1, 1, 0);
        grid.getChildren().add(open1);
        
        Button open2 = new Button("Open");
        GridPane.setConstraints(open2, 1, 1);
        grid.getChildren().add(open2);
        
        Button open3 = new Button("Open");
        GridPane.setConstraints(open3, 1, 2);
        grid.getChildren().add(open3);
        
        Button run = new Button("Compare Files");
        GridPane.setConstraints(run, 0, 3);
        grid.getChildren().add(run);
       
        // The action for the button to choose the master file. Contains error alerts.
        open1.setOnAction(new EventHandler<ActionEvent>() {
     
        	@Override
            public void handle(ActionEvent e) {
                chooseMasterFile();
            }
        	
        	public void chooseMasterFile(){
        		File masterFile = fileChooser.showOpenDialog(stage);
        		String masterPath = masterFile.getPath(); 
        		masterPath.toLowerCase();
        		if(!masterPath.contains("xlsx")){
        			Alert mAlert = new Alert(AlertType.ERROR);
        			mAlert.setTitle("Uh-oh!");
        			mAlert.setHeaderText("Wrong File Type");
        			mAlert.setContentText("It seems you have chosen something that is not an excel file!");
        			mAlert.show(); 
        		} else if(!masterPath.contains("master")){
        			Alert mAlert2 = new Alert(AlertType.ERROR);
        			mAlert2.setTitle("Uh-oh!");
        			mAlert2.setHeaderText("Wrong File Type");
        			mAlert2.setContentText("It seems you've chosen an unrelated file!");
        			mAlert2.show();
        		} else if(masterPath.contains("xlsx")){
        			input.setText(masterPath);
        		}
        	}
        });
       
        // The action for the button to choose the compared file. Contains error alerts.
        open2.setOnAction(new EventHandler<ActionEvent>() {
            
        	@Override
            public void handle(ActionEvent e) {
                chooseVendorFile();
            }
        	
        	public void chooseVendorFile(){
        		File vendorFile = secondFileChooser.showOpenDialog(stage);
        		String vendorPath = vendorFile.getPath();
        		vendorPath.toLowerCase();
        		if(!vendorPath.contains("xlsx")){
        			Alert vAlert = new Alert(AlertType.ERROR);
        			vAlert.setTitle("Uh-oh!");
        			vAlert.setHeaderText("Wrong File Type");
        			vAlert.setContentText("It seems you have chosen something that is not an excel file!");
        			vAlert.show(); 
        		} else if(!vendorPath.contains("vendor")){
        			Alert vAlert2 = new Alert(AlertType.ERROR);
        			vAlert2.setTitle("Uh-oh!");
        			vAlert2.setHeaderText("Wrong File Type");
        			vAlert2.setContentText("It seems you've chosen an unrelated file!");
        			vAlert2.show();
        		} else if(vendorPath.contains("xlsx")){
        			input2.setText(vendorPath);
        		}
        	}
        });
       
        
        // Chooses the destination folder for the new master sheet. Contains error alert...?
        open3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                File selectedDirectory = directoryChooser.showDialog(stage);
                if(selectedDirectory != null){
                	String finalPath = selectedDirectory.getPath();
                	input3.setText(finalPath);
                }
            }
        });
        
        // Runs the Comparison and creates the file
        run.setOnAction(new EventHandler<ActionEvent>() {
        	@Override
        	public void handle(ActionEvent e){
        		try{
        			FileInputStream master = new FileInputStream(input.getText());
					FileInputStream vendor = new FileInputStream(input2.getText());
					XSSFWorkbook masterBook = new XSSFWorkbook(master);
	    			XSSFWorkbook vendorBook = new XSSFWorkbook(vendor);
	    			XSSFSheet masterSheet = masterBook.getSheetAt(0);
	    			XSSFSheet vendorSheet = vendorBook.getSheetAt(0);


	    			
	    			// Read vendor sheet data
    			
	     			ArrayList<String> sku = new ArrayList<String>();
	    			ArrayList<String> description = new ArrayList<String>();
	    			ArrayList<String> map = new ArrayList<String>();
	    			String dataType;
	    			

	    			int vFirstRow = vendorSheet.getFirstRowNum();
	    			int vLastRow = vendorSheet.getLastRowNum();
	    			int vFirstCell = vendorSheet.getRow(0).getFirstCellNum();
	    			int vLastCell = vendorSheet.getRow(0).getLastCellNum();
	    			
	    			for(int j = vFirstCell; j < vLastCell; j++){
	    				XSSFRow rowM = vendorSheet.getRow(vFirstRow);
						XSSFCell cellM = rowM.getCell(j);
						String wow =cellM.getStringCellValue();
						if(wow.equals("SKU") || wow.equals("MAP") || wow.equals("Description")){
							dataType = wow;
						}else{
							continue;
						}
	    				for(int i = vFirstRow+1; i < vLastRow; i++){
	    					rowM = vendorSheet.getRow(i);
	    					cellM = rowM.getCell(j);
    						String inputM = null;
    						int type = cellM.getCellType();
    						//Iterator rowIter = masterSheet.rowIterator();
    						
    						switch(type){
    							case HSSFCell.CELL_TYPE_NUMERIC:
    								inputM = String.valueOf(cellM);
    								break;
    							case HSSFCell.CELL_TYPE_STRING:
    								inputM = cellM.getStringCellValue();
    								break;
    						}
    						
    						switch(dataType){
    							case "SKU":
    								sku.add(inputM);
    								break;
    							case "MAP":
    								map.add(inputM);
    								break;
    							case "Description":
    								description.add(inputM);
    								break;
    						}
 
    					}
    				}

	    			
	    			
	    			// Read master sheet data
	    			
	    			
	    			// Arraylist of every column as an arraylist
	    			// Arraylist of an array of Strings
	    			int mapMasterIndex = -1;
	    			int skuMasterIndex = -1;
	    			
	    			int firstRow = masterSheet.getFirstRowNum();
	    			int lastRow = masterSheet.getLastRowNum();
	    			int firstCell = masterSheet.getRow(0).getFirstCellNum();
	    			int lastCell = masterSheet.getRow(0).getLastCellNum();
	    			
	    			ArrayList<String[]> masterRowData = new ArrayList<String[]>(lastRow);   // All the row data	    			
	    			String[] tempFirstRow = new String[lastCell+1]; // Stores headers found in the first row

	    			// Set index values for both SKU and MAP to locate in excel sheet
	    			for(int i=firstCell;i<lastCell;i++) {
	    				XSSFRow rowM = masterSheet.getRow(firstRow);
	    				XSSFRow typeRowM = masterSheet.getRow(firstRow+1);
	    				String colName = rowM.getCell(i).getStringCellValue();
	    				tempFirstRow[i] = colName;
	    				switch(colName) {
	    					case "SKU":
	    						skuMasterIndex = i;
	    						break;
	    					case "MAP":
	    						mapMasterIndex = i;
	    						break;
	    					default:
	    						break;
	    					
	    				}
	    			}
	    			tempFirstRow[tempFirstRow.length-1] = "Detail";
	    			masterRowData.add(tempFirstRow);
	    			
	    			// Get all master sheet data and replace old data
	    			for(int x = firstRow+1; x < lastRow; x++){
		    			XSSFRow row = masterSheet.getRow(x);
	    				String[] tempRowData = new String[lastCell+1];   // The last array value holds detail
	    				for(int y = firstCell; y < lastCell; y++){
	    					XSSFCell cell = row.getCell(y);
	    					int type = cell.getCellType();
	    					String val = "";
    						switch(type){
							case HSSFCell.CELL_TYPE_NUMERIC:
								val = String.valueOf(cell);
								break;
							case HSSFCell.CELL_TYPE_STRING:
								val = cell.getStringCellValue();
								break;
							default:
								val = "="+String.valueOf(row.getCell(y));
						}
	    					tempRowData[y] =  val;
	    				}
	    				
	    				int vendorSKUIndex = sku.indexOf(tempRowData[skuMasterIndex]);   // Search for vendor in sku index
	    				if(sku.indexOf(tempRowData[skuMasterIndex]) != -1){    // Go here if sku is found in vendor sku arraylist
	    					int oldValue = (int) Double.parseDouble(tempRowData[mapMasterIndex]);
	    					int newValue = (int) Double.parseDouble(map.get(vendorSKUIndex));
	    					tempRowData[mapMasterIndex] = map.get(vendorSKUIndex);
	    					if(oldValue < newValue){
	    						tempRowData[tempRowData.length-1] = "MAP Increased";
        					}
	    					else if(oldValue > newValue) {
	    						tempRowData[tempRowData.length-1] = "MAP Decreased";
	    					}
	    					else {
	    						tempRowData[tempRowData.length-1] = "MAP Unchanged";
	    					}
	    				}
	    				else {
	    					tempRowData[tempRowData.length-1] = "Not Found";
	    				}
	    				
	    				masterRowData.add(tempRowData);
	    			}
	    			
	    			XSSFWorkbook outputBook = new XSSFWorkbook();
	    			XSSFSheet outputSheet = outputBook.createSheet("Master");
	    			XSSFSheet outputSheet2 = outputBook.createSheet("Detailed");
	    			
	    			for(int i = 0; i < masterRowData.size(); i++){
	    				XSSFRow row = outputSheet.createRow(i);
	    				String[] currentRow = masterRowData.get(i);
	    				for(int j = 0; j < currentRow.length-1; j++){
	    					XSSFCell finalCell = row.createCell(j);
	    					finalCell.setCellValue(currentRow[j]);
	    				}
	    			}
	    			
	    			for(int i = 0; i < masterRowData.size(); i++){
	    				XSSFRow row = outputSheet2.createRow(i);
	    				String[] currentRow = masterRowData.get(i);
	    				for(int j = 0; j < currentRow.length; j++){
	    					XSSFCell finalCell = row.createCell(j);
	    					
	    					finalCell.setCellValue(currentRow[j]);
	    				}
	    			}
	    			
	    				FileOutputStream out = new FileOutputStream(new File("test.xlsx"));
	    				outputBook.write(out);
	    				out.close();
	    				outputBook.close();
	    				
	    				Alert success = new Alert(AlertType.INFORMATION);
	    				success.setTitle("Success!");
	    				success.setHeaderText("You are success. (:");
	    				success.setContentText("File has been created!");
	    				success.show();
	    				
        	} catch (IOException e3){
        		PrintStream s = null;
				e3.printStackTrace(s);
				}

        	}
        });
        
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}